const { gql } = require('apollo-server');

/**
 * The schema below is defined to be an exact representation of data
 * from the Chuck Norris API
 */
export const typeDefs: string | string[] | any = gql`
"Query to get track array for the homepage grid"

type Query{
  getPeople(page: Int): People!
  getPerson(name: String): People!
}

type People {
  count: Int!
  next: String
  previous: String
  results: [ Person! ]!
}

type HomeWorld{
  name: String!
  climate: String!
  diameter: String!
  population: String!
}

type Person{
  name: String!
  mass: String!
  height: String!
  gender: String!
  homeworld: HomeWorld!
}
`;
