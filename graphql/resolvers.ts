const fetch = require('node-fetch');

/**
 * Resolvers define the technique for fetching the types defined in the
 * schema. This resolver retrieves people from https://api.chucknorris.io.
 **/
export const resolvers: any = {
  Query:{    
    getPeople: async (parent: any, {page}: any, {dataSources}) => dataSources.people.getPeople(page),
    getPerson: async (parent: any, { name }: any, { dataSources }) => dataSources.people.getPerson(name)
  },
  Person:{
    homeworld: async (person: any)=>{
      const response = await fetch(person.homeworld);
      return response.json();
    }
  }
};
