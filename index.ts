const { StarWarsAPI } = require('./datasources/people');
const {  ApolloServer } = require('apollo-server');
const {resolvers} = require('./graphql/resolvers')
const { typeDefs } = require('./graphql/schema')

/**
 * The ApolloServer constructor requires two parameters: the schema
 * definition and resolvers. Data sources also added since we are
 * querying an external API.
 */
//
const server: any = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => {
      return {
        people: new StarWarsAPI()
      }
    }
  }
);

// The `listen` method launches a web server serving the graphql endpoint.
server.listen().then(({ url }) => console.log(`🚀  Server ready at ${url}`));